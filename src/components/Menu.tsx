import styles from "./Menu.module.css";

interface MenuProps {
  active: boolean;
}

export default function Menu({ active }: MenuProps) {
  return (
    <>
      <nav style={active ? {} : { display: "none" }} className={styles.nav}>
        <ul>
          <li>
            <a href="/#">ENTRAR</a>
          </li>
          <li>
            <a href="/#">CURSOS</a>
          </li>
          <li>
            <a href="/#">SAIBA MAIS</a>
          </li>
        </ul>
      </nav>
    </>
  );
}
