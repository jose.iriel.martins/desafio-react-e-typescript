import { useEffect, useRef, useState } from "react";
import styles from "./List.module.css";

interface ListProps {
  title: string;
  items: string[] | string[][];
}

export default function List({ title, items }: ListProps) {
  let [active, setActive] = useState(false);
  let activeRef = useRef(false);

  useEffect(() => {
    activeRef.current = active;
  }, [active]);

  const isActive = () => {
    if (document.documentElement.clientWidth > 375) {
      return true;
    }
    return activeRef.current;
  };

  return (
    <div className={styles.list}>
      <h3
        onClick={(e) => {
          setActive(!active);
        }}
      >
        {title}
      </h3>
      {items.map((i) => {
        let element: JSX.Element;
        if (!Array.isArray(items[0])) {
          element = (
            <a style={isActive() ? {} : { display: "none" }} href="/#">
              {i}
            </a>
          );
        } else {
          element = (
            <>
              <h4 style={isActive() ? {} : { display: "none" }}>{i[0]}</h4>
              <p style={isActive() ? {} : { display: "none" }}>{i[1]}</p>
            </>
          );
        }

        return element;
      })}
    </div>
  );
}
