import { Formik, Field, Form } from "formik";
import * as Yup from "yup";

import styles from "./Newsletter.module.css";

export default function Newsletter() {
  return (
    <div className={styles.outer}>
      <Formik
        initialValues={{
          email: "",
        }}
        onSubmit={(values) => {}}
      >
        <Form>
          <label htmlFor="newletter-email" className={styles.title}>
            Assine nossa newletter
          </label>
          <div className={styles.field}>
            <Field
              required
              type="email"
              id="newsletter-email"
              name="newsletter-email"
              placeholder="E-mail"
            />
            <button className={styles.submit} type="submit">
              Enviar
            </button>
          </div>
        </Form>
      </Formik>
    </div>
  );
}
