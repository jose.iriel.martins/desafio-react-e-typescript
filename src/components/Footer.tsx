import styles from "./Footer.module.css";

import masterCard from "../images/Master.png";
import visa from "../images/Visa.png";
import americanExpress from "../images/Diners.png";
import elo from "../images/Elo.png";
import hiperCard from "../images/Hiper.png";
import payPal from "../images/Pagseguro.png";
import boleto from "../images/Boleto.png";

import vtexPci from "../images/vtex-pci-200.png";

// ?!
import vtexLogo from "../images/vtex-logo.svg";
import vtexV from "../images/vtex-v.svg";
import vtexT from "../images/vtex-t.svg";
import vtexE from "../images/vtex-e.svg";
import vtexX from "../images/vtex-x.svg";

import m3Logo from "../images/logo-m3.png";

export default function Footer() {
  return (
    <footer className={styles.footer}>
      <div className={styles.wrapper}>
        <p className={styles.blurb}>
          <span className={styles.blurbDesktop}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
          </span>
          <span className={styles.blurbMobile}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit.
          </span>
        </p>
        <div className={styles.cards}>
          <img className={styles.card} src={masterCard} alt="Mastercard" />
          <img className={styles.card} src={visa} alt="Visa" />
          <img className={styles.card} src={americanExpress} alt="American Express" />
          <img className={styles.card} src={elo} alt="Elo" />
          <img className={styles.card} src={hiperCard} alt="Hipercard" />
          <img className={styles.card} src={payPal} alt="PayPal" />
          <img className={styles.card} src={boleto} alt="Boleto bancário" />

          <div className={styles.separator}></div>

          <img className={styles.vtexPci} src={vtexPci} alt="VTEX / PCI certificado" />
        </div>
        <div className={styles.credits}>
          <div className={styles.poweredBy}>
            Powered by
            <div className={styles.vtexLogo}>
              <img src={vtexLogo} alt="VTEX" />
              <img src={vtexV} alt="" />
              <img src={vtexT} alt="" />
              <img src={vtexE} alt="" />
              <img src={vtexX} alt="" />
            </div>
          </div>
          <div className={styles.developedBy}>
            Developed by
            <img src={m3Logo} alt="M3" width="30" height="16" />
          </div>
        </div>
      </div>
    </footer>
  );
}
