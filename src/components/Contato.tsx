import { Formik, Field, Form, FormikProps } from "formik";
import * as Yup from "yup";

import styles from "./Contato.module.css";

interface FormValues {
  nome: string;
  email: string;
  cpf: string;
  nascimento: string;
  telefone: string;
  instagram: string;
  declaro: boolean;
}

const contatoSchema = Yup.object().shape({
  nome: Yup.string().min(2, "Curto demais").required("Obrigatório"),
  email: Yup.string().email("E-mail inválido").required("Obrigatório"),
  cpf: Yup.string()
    .matches(/^\d{3}[ \.]?\d{3}[ \.]?\d{3}[ \-]?\d{2}$/)
    .required("Obrigatório"),
  nascimento: Yup.string()
    .matches(/^([0-2]?[1-9]|[1-3][01]) ?\. ?(0?[1-9]|1[0-2]) ?\. ?\d{4}$/)
    .required("Obrigatório"),
  telefone: Yup.string()
    .matches(/^\(?\+?\d{0,2}\)? ?\d{4,5} ?\d{4}$/)
    .required("Obrigatório"),
  instagram: Yup.string().matches(/^@[\w\.]+$/),
  declaro: Yup.boolean(),
});

export default function Contato() {
  return (
    <div className={styles.outer}>
      <h2 className={styles.title}>Preencha o formulário</h2>

      <Formik
        initialValues={{
          nome: "",
          email: "",
          cpf: "",
          nascimento: "",
          telefone: "",
          instagram: "",
          declaro: false,
        }}
        validationSchema={contatoSchema}
        onSubmit={(values, { resetForm }) => {
          console.log(values);
          resetForm();
        }}
      >
        {({ errors, touched }: FormikProps<FormValues>) => {
          return (
            <Form>
              <label htmlFor="nome">Nome</label>
              <Field id="nome" name="nome" placeholder="Seu nome completo" />
              {touched.nome && errors.nome && <div className={styles.erro}>{errors.nome}</div>}

              <label htmlFor="email">E-Mail</label>
              <Field type="email" id="email" name="email" placeholder="Seu e-mail" />
              {touched.email && errors.email && <div className={styles.erro}>{errors.email}</div>}

              <label htmlFor="cpf">CPF</label>
              <Field type="tel" id="cpf" name="cpf" placeholder="000 000 000 00" />
              {touched.cpf && errors.cpf && <div className={styles.erro}>{errors.cpf}</div>}

              <label htmlFor="nascimento">Data de Nascimento</label>
              <Field id="nascimento" name="nascimento" placeholder="00 . 00 . 0000" />
              {touched.nascimento && errors.nascimento && (
                <div className={styles.erro}>{errors.nascimento}</div>
              )}

              <label htmlFor="telefone">Telefone</label>
              <Field type="tel" id="telefone" name="telefone" placeholder="(+00) 00000 0000" />
              {touched.telefone && errors.telefone && (
                <div className={styles.erro}>{errors.telefone}</div>
              )}

              <label htmlFor="instagram">Instagram</label>
              <Field id="instagram" name="instagram" placeholder="@seuuser" />
              {touched.instagram && errors.instagram && (
                <div className={styles.erro}>{errors.instagram}</div>
              )}

              <div className={styles.declaro}>
                <label htmlFor="declaro">
                  <span>*</span>Declaro que li e aceito
                </label>
                <Field required type="checkbox" id="declaro" name="declaro" />
              </div>
              {touched.declaro && errors.declaro && (
                <div className={styles.erro}>{errors.declaro}</div>
              )}

              <button className={styles.submit} type="submit">
                Cadastre-se
              </button>
            </Form>
          );
        }}
      </Formik>
    </div>
  );
}
