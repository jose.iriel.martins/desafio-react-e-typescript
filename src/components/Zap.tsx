import styles from "./Zap.module.css";

import whatsApp from "../images/whatsapp.svg";
import arrowUp from "../images/arrow-up.svg";

export default function Zap() {
  return (
    <div className={styles.items}>
      <div className={styles.item}>
        <div className={styles.whats}>
          <img src={whatsApp} alt="" />
        </div>
      </div>
      <div className={styles.item}>
        <div className={styles.arrowUp}>
          <img src={arrowUp} alt="" />
        </div>
      </div>
    </div>
  );
}
