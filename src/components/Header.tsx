import { useEffect, useRef, useState } from "react";

import Menu from "./Menu";

import styles from "./Header.module.css";

import logoM3 from "../images/logo-m3.png";
import carrinho from "../images/carrinho.svg";
import "../images/lupa.svg";

export default function Header() {
  let [burger, setBurger] = useState(true);
  let mobileMenu = useRef(false);

  useEffect(() => {
    mobileMenu.current = burger;
  });

  return (
    <header className={styles.header}>
      <div className={styles.top}>
        <div className={styles.burger}>
          <input
            onChange={(e) => {
              setBurger(!burger);
              console.log(
                `onChange:\n\t checkbox   = ${e.target.checked}\n\t burger     = ${burger}\n\t mobileMenu = ${mobileMenu.current}`
              );
            }}
            type="checkbox"
            id="burger-menu"
          />
          <label htmlFor="burger-menu">
            <span></span>
            <span></span>
            <span></span>
          </label>
        </div>

        <a className={styles.logo} href="/">
          <img src={logoM3} alt="M3" width="50" height="24" />
          <p>academy</p>
        </a>

        <input
          className={styles.busca}
          type="search"
          placeholder="Buscar..."
          name="header-busca"
          id="header-busca"
        />

        <div className={styles.misc}>
          <a href="/#" className={styles.entrar}>
            ENTRAR
          </a>
          <a href="/#">
            <img src={carrinho} alt="Carrinho de compras." />
          </a>
        </div>
      </div>

      <Menu active={mobileMenu.current} />

      <div className={styles.separator}></div>
    </header>
  );
}
