import facebook from "../images/facebook.svg";
import instagram from "../images/instagram.svg";
import twitter from "../images/twitter.svg";
import youtube from "../images/youtube.svg";
import linkedIn from "../images/linkedin.svg";

import styles from "./Links.module.css";

import List from "./List";

export default function Links() {
  return (
    <div className={styles.outer}>
      <div className={styles.lists}>
        <List
          title="Institucional"
          items={["Quem Somos", "Política de Privacidade", "Segurança", "Seja um Revendedor"]}
        />
        <List
          title="Dúvidas"
          items={["Entrega", "Pagamento", "Trocas e Devoluções", "Dúvidas Frequentes"]}
        />
        <List
          title="Fale conosco"
          items={[
            ["Atendimento ao Consumidor", "(11) 4159 9504"],
            ["Atendimento Online", "(11) 99433-8825"],
          ]}
        />
      </div>
      <div className={styles.final}>
        <div className={styles.social}>
          <div>
            <img src={facebook} alt="Facebook" />
          </div>
          <div>
            <img src={instagram} alt="Instagram" />
          </div>
          <div>
            <img src={twitter} alt="Twitter" />
          </div>
          <div>
            <img src={youtube} alt="YouTube" />
          </div>
          <div>
            <img src={linkedIn} alt="LinkedIn" />
          </div>
        </div>
        <a href="/#">www.loremipsum.com</a>
      </div>
    </div>
  );
}
