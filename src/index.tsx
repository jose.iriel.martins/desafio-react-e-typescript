import React from "react";
import ReactDOM from "react-dom/client";

import "./reset.css";
import "./global.css";

import Institucional from "./pages/Institucional";
import Header from "./components/Header";
import Newsletter from "./components/Newsletter";
import Links from "./components/Links";
import Footer from "./components/Footer";
import Zap from "./components/Zap";

const root = ReactDOM.createRoot(document.getElementById("root") as HTMLElement);

root.render(
  <React.StrictMode>
    <Header />
    <Institucional />
    <Newsletter />
    <Links />
    <Zap />
    <Footer />
  </React.StrictMode>
);
