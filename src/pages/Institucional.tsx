import { useState } from "react";

import styles from "./Institucional.module.css";
import Sobre from "../components/Sobre";
import Contato from "../components/Contato";

import home from "../images/home.svg";

export default function Institucional() {
  let [tab, setTab] = useState<"sobre" | "contato">("contato");

  let component: JSX.Element;
  switch (tab) {
    case "sobre":
      component = <Sobre />;
      break;
    case "contato":
      component = <Contato />;
      break;
  }

  return (
    <div className={styles.outer}>
      <div className={styles.crumbs}>
        <a href="/">
          <img src={home} alt="Home" />
        </a>
        <a href="/#">Institucional</a>
      </div>

      <h1 className={styles.title}>Institucional</h1>

      <div className={styles.inner}>
        <div className={styles.navWrapper}>
          <nav className={styles.nav}>
            <ul>
              <li>
                <button data-ativo={`${tab === "sobre"}`} onClick={(e) => setTab("sobre")}>
                  Sobre
                </button>
              </li>
              <li>
                <button>Forma de pagamento</button>
              </li>
              <li>
                <button>Entrega</button>
              </li>
              <li>
                <button>Troca e devolução</button>
              </li>
              <li>
                <button>Segurança e privacidade</button>
              </li>
              <li>
                <button data-ativo={`${tab === "contato"}`} onClick={(e) => setTab("contato")}>
                  Contato
                </button>
              </li>
            </ul>
          </nav>
        </div>
        <div className={styles.component}>{component}</div>
      </div>
    </div>
  );
}
